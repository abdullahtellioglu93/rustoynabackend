package com.rust.rustoyna.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rust.rustoyna.model.User;
import com.rust.rustoyna.repository.UserRepository;
import com.rust.rustoyna.util.ResponseException;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public User saveUser(User user) throws ResponseException {
		String email = user.getEmail();
		User emailUser = userRepository.findByEmail(email);
		if(emailUser != null) {
			throw new ResponseException("Email is not unique");
		}
		
		User save = userRepository.save(user);
		return save;
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}
}
