package com.rust.rustoyna.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rust.rustoyna.model.User;
import com.rust.rustoyna.model.dto.ErrorDTO;
import com.rust.rustoyna.repository.UserRepository;
import com.rust.rustoyna.service.UserService;
import com.rust.rustoyna.util.ResponseException;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService userService;
	
	@RequestMapping(value="/saveGet", method = RequestMethod.GET)
	public ResponseEntity<User> save(
			@RequestParam(name="userName", required=true) String userName,
			@RequestParam(name="password", required=true) String password,
			@RequestParam(name="steamId", required=false) String steamId,
			@RequestParam("email") String email
			) {
		User user = new User();
		user.setSteamId(steamId);
		user.setUserName(userName);
		user.setPassword(password);
		user.setEmail(email);
		try {
			userService.saveUser(user);	
		}catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
	@PostMapping("/savePost")
	public ResponseEntity<?> save(@RequestBody User saveUser){
		User returnUser = null;
		try {
			returnUser = userService.saveUser(saveUser);
		} catch (ResponseException e) {
			e.printStackTrace();
			return new ResponseEntity<>(new ErrorDTO(e.getDescription(),e.getErrorCode()),HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(returnUser, HttpStatus.OK);
	}
	
	@RequestMapping("/hello")
	public String helloWorld() {
		// http://localhost:8080/user/hello
		return "hello";
	}
	
	@RequestMapping("/findAll")
	public List<User> findAllUsers(){
		List<User> findAll = userService.findAll();
		return findAll;
	}
	//@PostMapping("/save")
	
	
	
}
