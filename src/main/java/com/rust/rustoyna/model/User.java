package com.rust.rustoyna.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rust.rustoyna.model.enums.RoleType;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="RID")
	private Integer id;
	@Column(name="RUSERNAME")
	private String userName;
	@Column(name="RPASSWORD")
	@JsonIgnore
	private String password;
	@Column(name="RUSERROLE")
	private RoleType userRole;
	@Column(name="RISBANNED")
	private Boolean isBanned;
	@Column(name="RSTEAMID")
	private String steamId;
	@Column(name="REMAIL")
	private String email;
	//TODO delete if not required
	@Column(name="RTOKEN")
	private String token;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public RoleType getUserRole() {
		return userRole;
	}
	public void setUserRole(RoleType userRole) {
		this.userRole = userRole;
	}
	public Boolean getIsBanned() {
		return isBanned;
	}
	public void setIsBanned(Boolean isBanned) {
		this.isBanned = isBanned;
	}
	public String getSteamId() {
		return steamId;
	}
	public void setSteamId(String steamId) {
		this.steamId = steamId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
	
	
}
