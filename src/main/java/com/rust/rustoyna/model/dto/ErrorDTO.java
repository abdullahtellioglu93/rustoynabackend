package com.rust.rustoyna.model.dto;

public class ErrorDTO {
	private String description;
	private Integer errorCode;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public ErrorDTO(String description, Integer errorCode) {
		super();
		this.description = description;
		this.errorCode = errorCode;
	}
	
	public ErrorDTO() {
		//NO - OP
	}
}
