package com.rust.rustoyna.model.enums;

public enum RoleType {
	PLAYER,
	ADMIN,
	OWNER
}
