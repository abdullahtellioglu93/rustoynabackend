package com.rust.rustoyna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RustoynaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RustoynaApplication.class, args);
	}

}

