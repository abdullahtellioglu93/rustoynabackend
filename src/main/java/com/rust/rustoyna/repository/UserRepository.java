package com.rust.rustoyna.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.rust.rustoyna.model.User;

@Repository
//user entity adi, Integer @Id annotationu altinda yazan degiskenin tipi
public interface UserRepository extends CrudRepository<User, Integer>{
	//Burada kod yazilmaz. method tanimi yapilabir
	
	
	//Bunu her sinifta yaz mutlaka
	@Override
	List<User> findAll();
	
	// @Query(nativeQuery=true, value="Select * ....")
	User findByEmail(String email);
	
}
