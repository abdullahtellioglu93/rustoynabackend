package com.rust.rustoyna.util;

public class ResponseException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private String description;
	private Integer errorCode;
	
	public ResponseException(String message) {
		super(message);
		this.description = message;
		// TODO Auto-generated constructor stub
	}
	public ResponseException(String description, Integer errorCode) {
		super(description);
		this.description = description;
		this.errorCode = errorCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
}
